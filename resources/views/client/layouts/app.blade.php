<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="/assets/images/layout/favicon.png">
    <title>
        @yield('title', 'My proyect')
    </title>

    {{-- FONTS --}}


    {{-- STYLES --}}
    @if ( config('app.env') === 'local' )
        <link rel="stylesheet" href="{{ mix('/css/plumcss.v1.2.3.css') }}">
        <link rel="stylesheet" href="{{ mix('/css/appDev.css') }}">
    @else
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    @endif
    @yield('styles')

</head>
<body>
    <div id="app" class="root-container">
        <!-- SITE LOADER -->
        <transition name="fade-slow">
            <div class="loader" v-show="!renderSite">
                <div class="loader-spinner"></div>
            </div>
        </transition>

        <!-- CONTENT -->
        <transition name="fade-slow">
            <template v-show="showSite">
                {{-- CONTENT --}}
                @yield('content')
            </template>
        </transition>
    </div>

    {{-- SCRIPTS --}}
    <script src="{{ mix('/js/app.js') }}"></script>
    @yield('scripts')
</body>
</html>
