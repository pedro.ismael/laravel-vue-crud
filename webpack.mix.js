const mix = require('laravel-mix');
require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if ( mix.inProduction() ){

    mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css')
    .version();

} else {

    mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/appDev.scss', 'public/css')
    .sass('resources/sass/plumcss.v1.2.3.scss', 'public/css')
    .version();

}
