<?php

Route::group(['namespace' => '\App\Http\Controllers\Client'], function() {
    foreach (scandir(__DIR__) as $file) {
        if (strpos($file, '.php') !== false && $file != 'client.php') require $file;
    }
});
